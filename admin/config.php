<?php
// HTTP
define('HTTP_SERVER', 'http://localhost/duan/trumhangmynew/admin/');
define('HTTP_CATALOG', 'http://localhost/duan/trumhangmynew/');

// HTTPS
define('HTTPS_SERVER', 'http://localhost/duan/trumhangmynew/admin/');
define('HTTPS_CATALOG', 'http://localhost/duan/trumhangmynew/');

// DIR
define('DIR_APPLICATION', '/Applications/MAMP/htdocs/duan/trumhangmynew/admin/');
define('DIR_SYSTEM', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/');
define('DIR_LANGUAGE', '/Applications/MAMP/htdocs/duan/trumhangmynew/admin/language/');
define('DIR_TEMPLATE', '/Applications/MAMP/htdocs/duan/trumhangmynew/admin/view/template/');
define('DIR_CONFIG', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/config/');
define('DIR_IMAGE', '/Applications/MAMP/htdocs/duan/trumhangmynew/image/');
define('DIR_CACHE', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/storage/cache/');
define('DIR_DOWNLOAD', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/storage/download/');
define('DIR_LOGS', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/storage/logs/');
define('DIR_MODIFICATION', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/storage/modification/');
define('DIR_UPLOAD', '/Applications/MAMP/htdocs/duan/trumhangmynew/system/storage/upload/');
define('DIR_CATALOG', '/Applications/MAMP/htdocs/duan/trumhangmynew/catalog/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_DATABASE', 'trumhangmynew');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
